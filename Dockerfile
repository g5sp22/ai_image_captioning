FROM python:3

# Install libgl1-mesa-glx
RUN apt-get update && apt-get install -y libgl1-mesa-glx

WORKDIR /code

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY src src

CMD [ "uvicorn", "src.main:app", "--host", "0.0.0.0", "--port","80", "--reload"]
