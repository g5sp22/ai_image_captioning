import cv2
import numpy as np
from tensorflow.keras.applications.inception_v3 import preprocess_input

# Chuẩn bị hình ảnh cho đầu vào mô hình
def preprocess_image(image):
    try:
        pic_size = 100   # Set the desired size for the image (InceptionV3 input size)
        image_data = image.read()
        image = cv2.imdecode(np.frombuffer(image_data, np.uint8), cv2.IMREAD_COLOR)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        image = cv2.resize(image, (pic_size, pic_size))
        image = preprocess_input(image)        
        image = np.expand_dims(image, axis=0)
        image = np.reshape(image, (1, pic_size, pic_size, 3))
        return image
    except Exception as e:
        print("An error occurred:", str(e))
        return None
