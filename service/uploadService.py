import shutil
from fastapi.responses import JSONResponse
import os
import uuid

def uploadFile(file):
    # try:
        # Lưu tệp vào thư mục cố định
        file_path = os.path.join("./src/upload/", str(uuid.uuid4())+file.filename)
        with open(file_path, "wb") as f:
            shutil.copyfileobj(file.file, f)
        return file_path
        # return JSONResponse(content={"message": "Tệp đã được lưu thành công." +file_path})
    # except Exception as e:
        # return JSONResponse(content={"message": "Lỗi khi lưu tệp.", "error": str(e)})