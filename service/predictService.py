from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing.sequence import pad_sequences
import numpy as np
from keras.applications.inception_v3 import InceptionV3
import pickle
import re
from service.preprocessImageService import preprocess_image

# Đường dẫn tới mô hình đã huấn luyện
model_path = './service/model/v1/model.h5'

# Đường dẫn tới tệp tokenizer
tokenizer_path = './service/model/v1/encoded_captions.pkl'

embedding_matrix_path = './service/model/v1/embedding_matrix.pkl'
with open(embedding_matrix_path, 'rb') as f:
    embedding_matrix = pickle.load(f)

# print(embedding_matrix.shape)
# # Function to load the tokenizer from the file
def load_tokenizer(tokenizer_path):
    with open(tokenizer_path, 'rb') as f:
        tokenizer = pickle.load(f)
    return tokenizer

# # # Tải mô hình và tokenizer
model = load_model(model_path)
tokenizer = Tokenizer()
tokenizer.word_index = load_tokenizer(tokenizer_path)
word_counts = {}
# Độ dài tối đa của mô tả
max_length = 0

for text in tokenizer.word_index.values():
   text1 = str(text)
   words = text1.split()
   max_length = len(words) if (max_length < len(words)) else max_length
   for w in words:
      try:
       word_counts[w] +=1
      except:
       word_counts[w] = 1
word_count_threshold = 10
vocab = [w for w in word_counts if word_counts[w] >= word_count_threshold]
i2w = {}
w2i = {}
id = 1
for w in vocab:
    w2i[w] = id
    i2w[id] = w
    id += 1
base_model = InceptionV3(include_top=False, weights='imagenet')

glove_dir = './service/model/v1/glove/'
embeddings_index = {} # empty dictionary
file = open(glove_dir + 'glove.6B.200d.txt', encoding="utf-8")

for line in file:
  values = line.split()
  word = values[0]
  coefs = np.asarray(values[1:], dtype='float32')
  embeddings_index[word] = coefs
file.close()

embedding_dim = 200
vocab_size = len(vocab) + 1 # thêm 1 padding
# Get 200-dim dense vector for each of the 10000 words in out vocabulary
embedding_matrix = np.zeros((vocab_size, embedding_dim))

for word, i in w2i.items():
    embedding_vector =embeddings_index.get(word)
    if embedding_vector is not None:
        # Words not found in the embedding index will be all zeros
        embedding_matrix[i] = embedding_vector

# # Tạo model
def remove_duplicate_words(text):
    words = text.split()
    unique_words = [words[i] for i in range(len(words)) if i == 0 or words[i] != words[i - 1]]
    return ' '.join(unique_words)

def greedy_search(model, embedding_matrix, i2w, w2i, image_features1, max_length):
    

    start_seq = 'startseq'
    image = base_model.predict(image_features1)
    image_features1 = image.reshape((image.shape[0], 2048))

    for _ in range(max_length):
        
        sequence = [w2i[w] for w in start_seq.split() if w in w2i]
        sequence = pad_sequences([sequence], maxlen=max_length, padding='post')
        # Nhúng các từ trong chuỗi sequence
        # print(embedding_vector)
        yhat = model.predict([image_features1, sequence], verbose=0)
        yhat = np.argmax(yhat)
        # word = i2w.get(yhat, None)
        word = i2w[yhat]
        # start_seq += ' ' + word
        if word is None or word == 'endseq':
            break
        
        start_seq += ' ' + word
    final = start_seq.split()[1:-1]
    # final = [word for final in final if word != "['startseq "]  
    final = ' '.join(final)
    final = start_seq.replace("['startseq ", '').strip()
    final = final.replace("startseq ", '').strip()
    final = final.replace("startseq", '').strip()
    final = final.replace("endseq'] ", '').strip()
    final = final.replace("endseq']", '').strip()
    final = remove_duplicate_words(final)
    return final

async def generate_description1(imageFile):
    image_features1 = preprocess_image(imageFile.file)
    if image_features1 is None:
        return {"error": "Error preprocessing image"}
    description = greedy_search(model, embedding_matrix,i2w, w2i, image_features1, max_length)
    file_path = "./src/description.txt"
    # data_list = description
    # Ghi danh sách vào tệp, mỗi phần tử trên một dòng
    with open(file_path, "w") as f:
        f.write(description)
    return description